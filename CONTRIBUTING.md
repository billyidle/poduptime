# How To Contribute

## Workflow

* Fork Git Repo https://gitlab.com/diasporg/poduptime
* Pull Git
* Create a topic branch to work from
* Commit and push your branch up
* Create a Merge Request to the develop branch

## Guidelines

* Note your changes in [`CHANGELOG.md`] following https://keepachangelog.com 
* Create any necessary DB migration script in [`db/migrations`] and note them in the changelog.
* Update [`README.md`] with needed changes
* Write your tests and validate them before you do your MR

## Coding Style

* PHP follows [PSR-12]
* CSS follows [CSSLint]
* JS follows [ESLint]

[`README.md`]: https://gitlab.com/diasporg/poduptime/blob/master/README.md
[`CHANGELOG.md`]: https://gitlab.com/diasporg/poduptime/blob/master/CHANGELOG.md
[`db/migrations`]: https://gitlab.com/diasporg/poduptime/tree/master/db/migrations
[Wiki]: https://gitlab.com/diasporg/poduptime/wikis/home
[PSR-12]: https://www.php-fig.org/psr/psr-12/
[CSSLint]: https://github.com/CSSLint/csslint
[ESLint]: https://eslint.org/docs/rules/
