<?php

/**
 * Main entry point for site.
 */

declare(strict_types=1);

use RedBeanPHP\R;
use Poduptime\PodStatus;
use RedBeanPHP\RedException;

require_once __DIR__ . '/boot.php';
$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);
$maximumoftwares = (int) $_SERVER['MAX_COUNT_SOFTWARES'];

$input           = isset($_GET['input']) ? urldecode(substr($_GET['input'], 1)) : null;
podLog('Uservisit, page: ' . $input);
$mapview         = isset($_GET['map']) || $input === 'map';
$search          = isset($_GET['search']) || $input === 'search';
$go              = isset($_GET['go']) || $input === 'go';
$listview        = isset($_GET['list']) || $input === 'list';
$termsview       = isset($_GET['terms']) || $input === 'terms';
$statsview       = isset($_GET['stats']) || $input === 'stats';
$dailystatsview  = isset($_GET['dailystats']) || $input === 'dailystats';
$podmin          = isset($_GET['podmin']) || $input === 'podmin';
$podminedit      = isset($_GET['podminedit']) || $input === 'podminedit';
$edit            = isset($_GET['edit']) || $input === 'edit';
$add             = isset($_GET['add']) || $input === 'add';
$gettoken        = isset($_GET['gettoken']) || $input === 'gettoken';
$status          = isset($_GET['status']) || $input === 'status';
$simpleview      = !($mapview || $podmin || $podminedit || $statsview);
$fullview        = false;
$subdomain       = join('.', explode('.', $_SERVER['HTTP_HOST'], -2));
$software_toggle = !empty($subdomain) ? $subdomain : 'All';
$software        = !empty($subdomain) ? ucwords($subdomain) : 'Fediverse';
$softwarejs      = !empty($subdomain) ? ucwords($subdomain) : '';
$softwaredb      = !empty($subdomain) ? $subdomain : 'all';
$software_all    = !empty($subdomain) ? ucwords($subdomain) : 'All';

if ($go) {
    include_once __DIR__ . '/app/helpers/go.php';
}

try {
    $softwares = R::getAll('
        SELECT softwarename,
        sum(total_users) AS users
        FROM pods
        WHERE status < ? 
        AND score > 0
        AND softwarename NOT SIMILAR TO ? 
        AND softwarename !~* ?
        GROUP BY softwarename
        ORDER BY users desc, softwarename
        LIMIT ?
    ', [PodStatus::RECHECK, $hiddensoftwares, '\'| |\.|/', $maximumoftwares]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

?>
<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <title><?php echo $_SERVER['TITLE'] ?></title>
    <meta name="keywords" content="fediverse, federated pods, Poduptime, diaspora, federated network, friendica, mastodon, open source social, open source social network"/>
    <meta name="description" content="<?php echo $software ?> Servers Status. Find a <?php echo $software ?> server to sign up for, find one close to you!"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/favicon.ico" type="image/x-icon">
    <link rel = "stylesheet" href = "<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/main.min.css" >
    <script src = "<?php echo $_SERVER['CDN_DOMAIN'] ?>node_modules/jquery/dist/jquery.min.js" ></script >
    <script defer src = "<?php echo $_SERVER['CDN_DOMAIN'] ?>node_modules/bootstrap/dist/js/bootstrap.bundle.min.js" ></script >
    <?php
    $listview && printf('<link rel = "stylesheet" href = "' . $_SERVER['CDN_DOMAIN'] . 'node_modules/tablesorter/dist/css/theme.bootstrap_4.min.css">');
    $edit && printf('<link rel = "stylesheet" href = "' . $_SERVER['CDN_DOMAIN'] . 'node_modules/ion-rangeslider/css/ion.rangeSlider.min.css">');
    $edit && printf('<script defer src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/ion-rangeslider/js/ion.rangeSlider.min.js"></script>');
    ?>
    <meta property="og:url" content="https://<?php echo $_SERVER['HTTP_HOST'] ?>/<?php echo $input ?>"/>
    <meta property="og:title" content="<?php echo $_SERVER['LONG_TITLE'] ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="<?php echo $software ?> Servers Status. Find a <?php echo $software ?> server to sign up for, find one close to you!"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
</head>
<body class="d-flex flex-column h-100">
    <nav class="shadow navbar navbar-expand-lg navbar-dark">
        <div class="container-fluid">
        <a class="text-dark navbar-brand fw-bold ms-2" href="/"><?php echo $_SERVER['TITLE'] ?></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item me-sm-2">
                    <a class="text-white nav-link" href="/list"><?php echo $t->trans('base.navs.list') ?></a>
                </li>
                <li class="nav-item active me-sm-2">
                    <a class="text-white nav-link fw-bold" href="/map"><?php echo $t->trans('base.navs.map') ?></a>
                </li>
                <li class="nav-item dropdown justify-content-center">
                    <?php
                    if ($_SERVER['DAILY_STATS']) {
                        echo '<a class="text-white nav-link dropdown-toggle" href="#" id="navbarDropdownMenuStats" data-bs-toggle="dropdown" role="button" aria-expanded="false">' . $t->trans('base.navs.stats') . '</a>';
                        echo '<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuStats">';
                        echo '<a class="dropdown-item" href="/stats")>' . $t->trans('base.navs.monthlystats') . '</a>';
                        echo '<a class="dropdown-item" href="/dailystats")>' . $t->trans('base.navs.dailystats') . '</a>';
                        echo '</div>';
                    } else {
                        echo '<a class="text-white nav-link" href="/stats">' . $t->trans('base.navs.stats') . '</a>';
                    }
                    ?>
                </li>
                <span class="text-white navbar-text justify-content-center ms-lg-3">
                <?php echo $t->trans('base.navs.software') ?>:
                </span>
                <li class="nav-item dropdown justify-content-center">
                    <a class="text-white nav-link dropdown-toggle" href="#" id="navbarDropdownMenuSoftwares" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                        <?php echo $software_toggle ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuSoftwares">
                        <a class="dropdown-item" href="//<?php echo $_SERVER['DOMAIN'] . strtok($_SERVER["REQUEST_URI"], '?') ?>">All</a>
                        <?php
                        foreach ($softwares as $software) {
                                printf(
                                    '<a class="dropdown-item" href="//%1$s.' . $_SERVER['DOMAIN'] . strtok(urldecode($_SERVER["REQUEST_URI"]), '?') . '">%1$s</a> ',
                                    $software['softwarename']
                                );
                        }
                        ?>
                    </div>
                </li>
            </ul>
            <form class="d-flex ms-auto" action="/search" method="get">
                <input id="search" class="form-control me-2" name="query" type="search" placeholder="<?php echo $t->trans('base.navs.search') ?>" aria-label="Search">
            </form>
        </div>
        </div>
    </nav>
</div>
<main class="flex-shrink-0">
    <div class="main">
        <?php
        if ($mapview) {
            include_once __DIR__ . '/app/views/showmap.php';
        } elseif ($statsview) {
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/chart.js/dist/chart.umd.js"></script>');
            include_once __DIR__ . '/app/views/statsview.php';
        } elseif ($dailystatsview) {
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/chart.js/dist/chart.umd.js"></script>');
            include_once __DIR__ . '/app/views/dailystatsview.php';
        } elseif ($termsview) {
            include_once __DIR__ . '/app/views/termsview.php';
        } elseif ($search) {
            include_once __DIR__ . '/app/views/search.php';
        } elseif ($listview) {
            printf('<script defer src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/tablesorter/dist/js/jquery.tablesorter.combined.min.js"></script>');
            printf('<script defer src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/tablesorter/dist/js/extras/jquery.tablesorter.pager.min.js"></script>');
            printf('<script defer src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/tablesorter/dist/js/widgets/widget-columnSelector.min.js"></script>');
            printf('<script defer src="' . $_SERVER['CDN_DOMAIN'] . 'app/assets/js/poduptime.fullview.min.js"></script>');
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/chart.js/dist/chart.umd.js"></script>');
            include_once __DIR__ . '/app/views/showfull.php';
        } elseif ($podmin) {
            include_once __DIR__ . '/app/views/podmin.php';
        } elseif ($status) {
            include_once __DIR__ . '/app/views/status.php';
        } elseif ($podminedit) {
            include_once __DIR__ . '/app/views/podminedit.php';
        } elseif ($edit) {
            include_once __DIR__ . '/app/functions/edit.php';
        } elseif ($add) {
            include_once __DIR__ . '/app/functions/add.php';
        } elseif ($gettoken) {
            include_once __DIR__ . '/app/functions/gettoken.php';
        } elseif ($termsview) {
            include_once __DIR__ . '/app/views/termsview.php';
        } elseif ($input) {
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/chart.js/dist/chart.umd.js"></script>');
            include_once __DIR__ . '/app/views/singleview.php';
        } else {
            include_once __DIR__ . '/app/views/welcome.php';
        }
        ?>
    </div>
</main>
<footer class="text-white footer mt-auto py-3">
    <div class="container">
        <div class="row-cols-1">
                    <?php
                    $csoftwares = c('softwares');
                    foreach ($csoftwares as $csoftware => $details) {
                        if ($csoftware === $subdomain && $details['info']) {
                            echo '<div class="row">
                                  <h5 class="text fw-bold d-flex justify-content-center">
                                  <a class="white d-flex justify-content-center" target="_new" href="' . $details['info'] . '">About ' . $subdomain . '</a></h5><p class="d-flex justify-content-center">';
                            echo $t->trans('softwares.' . $subdomain);
                            echo '</p></div>';
                        }
                    }
                        echo '
                    <div class="row d-flex justify-content-center">
                    <h5 class="text fw-bold d-flex justify-content-center">About ' . $_SERVER['TITLE'] . '</h5>
                    <p class="d-flex justify-content-center">' . $t->trans('base.strings.about') . '</p>
                    </div>
                    ';
                    ?>
        </div>
    </div>
            <div class="d-flex justify-content-center">
                <div class="ps-2 col-md-auto"><a class="text-white-50" href="/podmin"><?php echo $t->trans('base.navs.add') ?></a></div>
                <div class="ps-2 col-md-auto"><a class="text-white-50" href="/podminedit"><?php echo $t->trans('base.navs.edit') ?></a></div>
                <div class="ps-2 col-md-auto"><a class="text-white-50" href="/terms"><?php echo $t->trans('base.navs.terms') ?></a></div>
                <div class="ps-2 col-md-auto"><a class="text-white-50" href="https://gitlab.com/diasporg/poduptime"><?php echo $t->trans('base.navs.source') ?></a></div>
                <div class="ps-2 col-md-auto"><a class="text-white-50" href="<?php echo  $_SERVER['API_LOCATION'] ?>"><?php echo $t->trans('base.navs.api') ?></a></div>
                <div class="ps-2 col-md-auto"><a class="text-white-50" href="/status"><?php echo $t->trans('base.navs.status') ?></a></div>
                <?php
                if ($_SERVER['SERVER_AD_TEXT']) {
                    echo "<div class='ps-2 col-md-auto'><a class='text-white-50' href='" . $_SERVER['SERVER_AD_URL'] . "'>" . $_SERVER['SERVER_AD_TEXT'] . "</a></div>";
                }
                ?>
            </div>
</footer>
<input type="hidden" name="software" value="<?php echo lcfirst($softwarejs) ?>">
<?php
$statsview && include_once __DIR__ . '/app/views/statsviewjs.php';
$dailystatsview && include_once __DIR__ . '/app/views/dailystatsviewjs.php';
?>
<link rel = "stylesheet" href = "<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/second.min.css" >
</body></html>
