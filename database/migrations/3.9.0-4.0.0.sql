CREATE TABLE dailystats (
    id serial8 UNIQUE PRIMARY KEY,
    softwarename text,
    total_users int,
    total_active_users_halfyear int,
    total_active_users_monthly int,
    total_posts int,
    total_comments int,
    total_pods int,
    total_uptime int,
    date_checked timestamp DEFAULT current_timestamp
);