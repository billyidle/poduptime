<?php

/**
 * Pull sourcecode versions from git repos
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

masterVersionCrawl();

addMeta('masterversions_updated');

podLog('Masterversions Update');
