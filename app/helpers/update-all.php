<?php

/**
 * Pull pod info in parallel for all pods in database
 * Use php-cgi or set higher time limits in php.ini
 */

declare(strict_types=1);

set_time_limit(0);

require_once __DIR__ . '/../../boot.php';

if (!isCli()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

use Carbon\Carbon;
use Poduptime\UpdateServerTask;
use Spatie\Async\Pool;

if (getMeta('pods_updating')) {
    die('already running');
}

if (!isConnected()) {
    die('no internet');
}

$time_start = microtime(true);

if (getMeta('pods_updating')) {
    die('already running');
}
addMeta('pods_updating', true);

$servers = allDomainsData() ?? [];

$pool = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);

foreach (array_column($servers, 'domain') as $domain) {
    $pool
        ->add(new UpdateServerTask($domain))
        ->catch(function ($exception) use ($domain) {
            podLog('error on updating ' . $exception, $domain, 'error');
        });
}

$pool->forceSynchronous()->wait();

addMeta('pods_updated');

$time_end       = microtime(true);
$execution_time = ($time_end - $time_start) / 60;
addMeta('pods_update_runtime', round($execution_time));
addMeta('pods_updating', false);
