<?php

/**
 * Create a basic sitemap.xml file to help site get indexed
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

if (!isCli()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

//new servers found rss
file_put_contents($_SERVER['BASE_DIR'] . '/' . $_SERVER['NEW_SERVERS_RSS'], updateRSS());

podLog('RSS feed updated');
addMeta('rssmap_updated');

//update git verion
file_put_contents($_SERVER['BASE_DIR'] . '/tag.txt', exec('git describe --tags --abbrev=0'));

podLog('poduptime version updated');
addMeta('tagfile_updated');

//update sitemap
updateSitemap();

podLog('sitemap.xml updated');
addMeta('sitemap_updated');
