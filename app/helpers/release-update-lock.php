<?php

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

if (!isCli()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

if (getMeta('pods_updating')) {
    addMeta('pods_updating', false);
    echo "lock removed";
} else {
    echo "not locked";
}
