<?php

/**
 * Compute daily stats.
 */

declare(strict_types=1);

use RedBeanPHP\R;
use RedBeanPHP\RedException as RedExceptionAlias;

require_once __DIR__ . '/../../boot.php';

if (!isCli()) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

try {
    $total_runs = R::getAll("
        SELECT
            count(value), 
            to_char(date_created, 'yyyy-mm-dd') as yymmdd
        FROM meta
        WHERE name = 'pods_updating' AND value = '1'
        GROUP BY yymmdd
    ");
} catch (RedExceptionAlias $e) {
    podLog('Error in SQL select query' . $e->getMessage(), '', 'error');
}

try {
    $daily_totals_all = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm-dd') AS yymmdd,
            sum(total_users) as users,
            sum(active_users_halfyear) as active_users_halfyear,
            sum(active_users_monthly) as active_users_monthly,
            sum(local_posts) as posts,
            sum(comment_counts) as comments,
            count(domain) as pods,
            count(nullif(online, false)) as uptime
        FROM checks
        WHERE online = true
        GROUP BY yymmdd
        ORDER BY yymmdd DESC 
    ");
} catch (RedExceptionAlias $e) {
    podLog('Error in SQL select query' . $e->getMessage(), '', 'error');
}

foreach ($daily_totals_all as $daily) {
    // Format date to timestamp.
    $timestamp = $daily['yymmdd'] . ' 01:01:01-01';
    $total = 1;
    foreach ($total_runs as $runs) {
        if ($daily['yymmdd'] === $runs['yymmdd']) {
            $total = $runs['count'];
            break;
        }
    }

    try {
        $p = R::findOrCreate('dailystats', ['date_checked' => $timestamp, 'softwarename' => 'all']);
        $p['softwarename']                  = 'all';
        $p['total_users']                   = round($daily['users'] / $total);
        $p['total_active_users_halfyear']   = round($daily['active_users_halfyear'] / $total);
        $p['total_active_users_monthly']    = round($daily['active_users_monthly'] / $total);
        $p['total_posts']                   = round($daily['posts'] / $total);
        $p['total_comments']                = round($daily['comments'] / $total);
        $p['total_pods']                    = round($daily['pods'] / $total);

        R::store($p);
    } catch (RedExceptionAlias $e) {
        podLog('Error in SQL query at insert monthly for all' . $e->getMessage(), 'all', 'error');
    }
}

try {
    $daily_totals_bypod = R::getAll("
       SELECT
            to_char(checks.date_checked, 'yyyy-mm-dd') AS yymmdd,
            sum(checks.total_users) as users,
            sum(checks.active_users_halfyear) as active_users_halfyear,
            sum(checks.active_users_monthly) as active_users_monthly,
            sum(checks.local_posts) as posts,
            sum(checks.comment_counts) as comments,
            count(checks.domain) as pods,
            count(nullif(checks.online, false)) as uptime,
            pods.softwarename as softwarename
        FROM checks
        INNER JOIN pods ON pods.domain = checks.domain 
        WHERE online = true
        GROUP BY yymmdd,pods.softwarename
        ORDER BY yymmdd DESC 
    ");
} catch (RedExceptionAlias $e) {
    podLog('Error in SQL select query' . $e->getMessage(), '', 'error');
}

foreach ($daily_totals_bypod as $daily) {
    // Format date to timestamp.
    $timestamp = $daily['yymmdd'] . ' 01:01:01-01';
    $total = 1;
    foreach ($total_runs as $runs) {
        if ($daily['yymmdd'] === $runs['yymmdd']) {
            $total = $runs['count'];
            break;
        }
    }

    try {
        $p = R::findOrCreate('dailystats', ['date_checked' => $timestamp, 'softwarename' => $daily['softwarename']]);
        $p['softwarename']                = $daily['softwarename'];
        $p['total_users']                 = $p['total_users']                  = round($daily['users'] / $total);
        $p['total_active_users_halfyear'] = $p['total_active_users_halfyear']  = round($daily['active_users_halfyear'] / $total);
        $p['total_active_users_monthly']  = $p['total_active_users_monthly']   = round($daily['active_users_monthly'] / $total);
        $p['total_posts']                 = $p['total_posts']                  = round($daily['posts'] / $total);
        $p['total_comments']              = round($daily['comments'] / $total);
        $p['total_pods']                  = round($daily['pods'] / $total);

        R::store($p);
    } catch (RedExceptionAlias $e) {
        podLog('Error in SQL query at insert monthly for a software' . $e->getMessage(), $daily['softwarename'], 'error');
    }
}

podLog('daily stats updated');
addMeta('daily_statstable_updated');
