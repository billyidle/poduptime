<?php

/**
 * Form to add a new pod.
 */

declare(strict_types=1);

?>

<p class="p-2"><?php echo $t->trans('admin.add') ?></p>
<form action="/">
    <div class="container-fluid">
    <div class="form-group row">
        <label for="domain-input" class="col-2 col-form-label"><?php echo $t->trans('admin.domain') ?> *</label>
        <div class="col-md-4">
            <div class="input-group mb-2 me-sm-2 mb-sm-0">
                <div class="input-group-addon"></div>
                <input type="text" id="domain-input" name="domain" class="form-control" placeholder="domain.com" aria-describedby="domain-help" aria-required="true" required>
            </div>
            <small id="domain-help" class="form-text text-muted"><?php echo $t->trans('admin.domainnote') ?>.</small>
        </div>
    </div>
    <div class="form-group row">
        <label for="email-input" class="col-2 col-form-label"><?php echo $t->trans('admin.email') ?></label>
        <div class="col-md-4">
            <input type="email" id="email-input" name="email" class="form-control" placeholder="user@domain.com" aria-describedby="email-help">
            <small id="email-help" class="form-text text-muted"><?php echo $t->trans('admin.emailnote') ?></small>
        </div>
    </div>
    <br>
    <input type="hidden" name="add">
    <button type="submit" class="btn btn-primary" name="action" value="save"><?php echo $t->trans('base.general.submit') ?></button>
</form>
</div>
