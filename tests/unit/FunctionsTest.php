<?php

use Codeception\Test\Unit;
use Poduptime\AddServerTask;
use Poduptime\UpdateServerTask;

class FunctionsTest extends Unit
{
    public function testConfigHelper()
    {
        $this->assertIsArray(c());
        $this->assertStringContainsString('diaspora/diaspora', c('softwares')['diaspora']['repo']);
        $this->assertNull(c('invalid_key'));
        $this->assertSame('default', c('invalid_key', 'default'));
    }

    public function testCurl()
    {
        $curl = curl('icanhazip.com');
        $this->assertEquals(200, $curl['code']);
    }

    public function testRobotAllowTrue()
    {
        $domain = 'xerox.com';
        $path = '/about';
        $try = robotAllowed($domain, $path);
        $this->assertTrue($try);
    }

    public function testRobotAllowFalse()
    {
        $domain = 'xerox.com';
        $path = '/cgi-bin/imagejerherkeh.png';
        $try = robotAllowed($domain, $path);
        $this->assertFalse($try);
    }

    public function testUpdateMeta()
    {
        require_once __DIR__ . '/../../boot.php';
        $meta = addMeta('test', '42');
        $this->assertGreaterThanOrEqual(1, $meta);
    }

    public function testGetMeta()
    {
        require_once __DIR__ . '/../../boot.php';
        $meta = getMeta('test');
        $this->assertEquals('42', $meta);
    }

    public function testIpData()
    {
        $ipData = ipData('diasp.org');
        $this->assertEquals('170.39.215.215', $ipData['ipv4'][0]);
        $this->assertEquals('2604:f440:2:5::215', $ipData['ipv6'][0]);
        $this->assertNotEmpty($ipData['txt']);
    }

    public function testIpLocation()
    {
        $ipLocation = ipLocation('2.125.160.216');
        $this->assertNotEmpty($ipLocation['zipcode']);
    }

    public function testIpDataIp4Only()
    {
        $ipData = ipData('ipv4.icanhazip.com');
        $this->assertArrayHasKey('ipv4', $ipData);
        $this->assertArrayNotHasKey('ipv6', $ipData);
    }

    public function testIpDataIp6Only()
    {
        $ipData = ipData('ipv6.icanhazip.com');
        $this->assertArrayHasKey('ipv6', $ipData);
        $this->assertArrayNotHasKey('ipv4', $ipData);
    }

    public function testSecondsToTime()
    {
        $this->assertSame('0 hours, 1 minutes', secondsToTime(-60));
        $this->assertSame('0 hours, 0 minutes', secondsToTime(0));
        $this->assertSame('0 hours, 0 minutes', secondsToTime(59));
        $this->assertSame('0 hours, 1 minutes', secondsToTime(60));
        $this->assertSame('1 hours, 0 minutes', secondsToTime(3_600));
        $this->assertSame('11 hours, 11 minutes', secondsToTime(40_260));
        $this->assertSame('999 hours, 59 minutes', secondsToTime(3_599_940));
        $this->assertSame('1h 2m 3s', secondsToTime(3723, '%3$dh %2$dm %1$ds'));
    }

    /**
     * @dataProvider curlPodsProviders
     */
    public function testExtractPodsFromCurl($curl, $field, $column, $pods)
    {
        $this->assertSame($pods, extractPodsFromCurl($curl, $field, $column));
    }

    public function curlPodsProviders(): \Generator
    {
        // https://diasp.org/pods.json
        yield [
            ['body' => '[{"host":"one.pod"},{"host":"two.pod"}]'],
            null,
            'host',
            ['one.pod', 'two.pod'],
        ];

        // https://the-federation.info/pods.json
        yield [
            ['body' => '{"pods":[{"host":"one.pod"},{"host":"two.pod"}]}'],
            'pods',
            'host',
            ['one.pod', 'two.pod'],
        ];

        // https://fedidb.org/api/v0/network/instances
        yield [
            ['body' => '["one.pod","two.pod"]'],
            null,
            null,
            ['one.pod', 'two.pod'],
        ];

        // https://instances.social/api/1.0/instances/list?count=0
        yield [
            ['body' => '{"instances":[{"name":"one.pod"},{"name":"two.pod"}]}'],
            'instances',
            'name',
            ['one.pod', 'two.pod'],
        ];
    }

    public function testNodeinfo()
    {
        $domain = nodeInfo('diasp.org');
        $this->assertNotEmpty($domain['body']);
        $this->assertIsString($domain['sslexpire']);
        $this->assertIsNumeric($domain['httpcode']);
    }

    public function testcheckNodeinfo()
    {
        $domain = checkNodeinfo('diasp.org');
        $this->assertTrue($domain);
    }

    public function testcleanDomain()
    {
        $domain = cleanDomain('@diasp.org/');
        $this->assertEquals('diasp.org', $domain);
    }

    public function testMasterVersionCrawl()
    {
        $outcome = masterVersionCrawl();
        $this->assertTrue($outcome);
    }

    public function testISConnected()
    {
        $domain = isConnected();
        $this->assertTrue($domain);
    }

    public function testBackupData()
    {
        $backup = backupData();
        $this->assertFileExists($backup);
    }

    public function testWriteCheck()
    {
        $write = writeCheck(["taco.gov", true, "555", "55.55555", "555", "54", "3", "555", "555", "5.5.5", "5.5.5-git42"]);
        echo $write;
        $this->assertTrue($write);
    }

    public function testReadCheck()
    {
        $read = readCheck("taco.gov", true);
        $this->assertStringContainsStringIgnoringCase('5.5.5', $read['version']);
    }

    public function testPodLog()
    {
        podLog('tacotuesday', 'taco.gov');
        $this->assertFileExists('log/script-' . $_SERVER['APP_ENV'] . '.log');
    }

    public function testIsCli()
    {
        $cli = isCli();
        $this->assertTrue($cli);
    }

    public function testTxtToQuery()
    {
        $newquery = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
        $this->assertEquals('%(nginx|wordpress)%', $newquery);
    }

    public function testAddServerWithPoolIpv6Only()
    {
        $added = addServer('ipv6.diasp.net');
        $this->assertTrue($added);
    }

    public function testAddServerWithPool()
    {
        $added = addServer('diasp.org');
        $this->assertTrue($added);
    }

    public function testAddServer()
    {
        $task = new AddServerTask('diasp.eu');
        $task();
        $this->assertStringContainsStringIgnoringCase('diasp.eu', file_get_contents('log/script-' . $_SERVER['APP_ENV'] . '.log'));
    }

    public function testUpdateServerWithPoolIpv6Only()
    {
        $updated = updateServer('ipv6.diasp.net');
        $this->assertTrue($updated);
    }

    public function testUpdateServerWithPool()
    {
        $updated = updateServer('diasp.org');
        $this->assertTrue($updated);
    }

    public function testUpdateServer()
    {
        $task = new UpdateServerTask('diasp.eu');
        $task();
        $this->assertStringContainsStringIgnoringCase('diasp.eu', file_get_contents('log/script-' . $_SERVER['APP_ENV'] . '.log'));
    }

    public function testallServersList()
    {
        $servers = allServersList('diaspora');
        $this->assertArrayHasKey('0', $servers);
    }

    public function testallDomainsData()
    {
        $servers = allDomainsData(null, true);
        $this->assertIsNumeric($servers[0]['count']);
    }

    public function testUpdateSitemap()
    {
        updateSitemap();
        $this->assertFileExists($_SERVER['SITEMAP']);
    }

    public function testClosestServers()
    {
        $servers = closestServers('2.125.160.216', 8);
        $this->assertIsArray($servers);
    }

    public function testUpdateRSS()
    {
        $atom = updateRSS();
        $this->assertStringContainsStringIgnoringCase('diasp.eu', $atom);
    }
}
