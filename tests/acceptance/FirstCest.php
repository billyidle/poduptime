<?php

class FirstCest
{
    public function _before(AcceptanceTester $I)
    {
    }
    public function FrontPageLoads(AcceptanceTester $I)
    {
        // index.php loads welcome.php properly
        $I->amOnPage('/');
        $I->seeInSource('fediverse home');
    }

    public function MapPageLoads(AcceptanceTester $I)
    {
        // showmap.php loads map properly
        $I->amOnPage('/?map');
        $I->seeInSource('OpenStreetMap');
    }

    public function AddFail(AcceptanceTester $I)
    {
        // database/add.php fail on bad domain
        $I->amOnPage('/app/functions/add.php?domain=xerox.com');
        $I->see('Could not validate');
    }

    public function AddFailDupe(AcceptanceTester $I)
    {
        // database/add.php fail on dupe domain
        $I->amOnPage('/app/functions/add.php?domain=diasp.org');
        $I->see('Server already exists');
    }

    public function CheckTableData(AcceptanceTester $I)
    {
        // validate server now on raw list data
        $I->amOnPage('/?list');
        $I->seeInSource('loadingmessage');
    }

    public function CheckSearch(AcceptanceTester $I)
    {
        // validate server now on search results
        $I->amOnPage('/app/views/search.php?query=diasp.org');
        $I->see('diasp.org');
    }

    public function PodminEdit(AcceptanceTester $I)
    {
        //fill for and push button. php server won't do action like nginx would
        $I->amOnPage('/app/views/podminedit.php');
        $I->submitForm('#podminedit', ['domain' => 'diasp.org'], '.btn-primary');
        $I->seeInCurrentUrl('/?domain');
    }

    public function ApiMainpage(AcceptanceTester $I)
    {
        // validate api index loads
        $I->amOnPage('/api/');
        $I->seeInSource('diasp.org');
    }
}
